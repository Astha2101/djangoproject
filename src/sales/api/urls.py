from django.urls import path
from django.urls.resolvers import URLPattern
from sales.api.views import ApiSaleListView,ApiSalesDetailView

app_name='sales'

urlpatterns=[
    path('', ApiSaleListView.as_view(), name='list'),
    path('<pk>/',ApiSalesDetailView, name='detail'),
]