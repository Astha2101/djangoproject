from rest_framework import serializers
from sales.models import Position,Sale,CSV

class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model=Position
        fields=['quantity','price','created']

class SaleSerializer(serializers.ModelSerializer):
    customer=serializers.SerializerMethodField('get_name_from_customer')
    salesman=serializers.SerializerMethodField('get_name_from_salesman')
    products=serializers.SerializerMethodField('get_products')
    class Meta:
        model=Sale
        fields=['transaction_id','total_price','created','updated','customer','salesman','products']
    def get_name_from_customer(self,sale):
        name=sale.customer.name
        return name
    def get_name_from_salesman(self,sale):
        name=sale.salesman.user.username
        return name
    def get_products(self,sale):
        lst=[]
        for pos in sale.get_positions():
            l=[]
            l.append(pos.product.name)
            l.append(pos.quantity)
            l.append(pos.price)
            l.append(pos.get_sales_customer())
            lst.append(l)
        return lst








class CSVSerializer(serializers.ModelSerializer):
    class Meta:
        model=CSV
        fields=['file_name','csv_file','created','updated']