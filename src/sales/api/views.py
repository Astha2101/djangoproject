from rest_framework import serializers, status
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView

from sales.api.serializers import SaleSerializer
from customer.api.serializers import CustomerSerializer
from profiles.api.serializers import ProfileSerializer
from products.api.serializers import ProductSerializer

from sales.models import Sale

class ApiSaleListView(ListAPIView):
    queryset=Sale.objects.all()
    print(queryset)
    serializer_class=SaleSerializer
@api_view(['GET', ])
def ApiSalesDetailView(request,pk):
    try:
        sales=Sale.objects.get(pk=pk)
    except Sale.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer=SaleSerializer(sales)
    return Response(serializer.data)









