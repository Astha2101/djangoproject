

# Introduction

This is a sales report data generator web application built using Django framework. Libraries used are pandas, dataframes, matplotlib, seaborn integration, pdf integration, java ajax integration and so on. I have also included a api folder in which I have developed a Rest Api using Django Rest Framework.

### ScreenShots

* Home View
![Default Home View](screenshots/ss1.png?raw=true "Title")

* Search 
![Search View](screenshots/ss2.png?raw=true "Title")

* Charts
![Search_view_chart](screenshots/ss3.png?raw=true "Title")

* Add Report
![Add_Report_View](screenshots/ss1.png?raw=true "Title")

* Sales List
![SALES_LIST_VIEW](screenshots/SS5.png?raw=true "Title")

* Sales Detail
![sales_detail_view](screenshots/ss6.png?raw=true "Title")

* Upload csv
![upload_csv_view](screenshots/ss7.png?raw=true "Title")

* Reports List
![report_view](screenshots/ss8.png?raw=true "Title")

* Report Detail
![report_detail_view](screenshots/ss9.png?raw=true "Title")

* Generate Pdf
![pdf_view](screenshots/ss10.png?raw=true "Title")

### Main features

* Generate sales report

* Upload CSV files and add records

* View details of sale

* Download sales report as Pdfs

* Add detailed report

* Generate different statistics diagrams for sales data

# Usage

Can be used to store sales data and visualize data and generate reports.



# Django Project 

# Getting Started

First clone the repository from Github and switch to the new directory:

    $ git clone https://Astha2101@bitbucket.org/Astha2101/djangoproject.git
    $ cd src
    
Activate the virtualenv for your project.
    
Install project dependencies:

    $ pip install -r requirements/local.txt
    
    
Then simply apply the migrations:

    $ python manage.py migrate
    

You can now run the development server:

    $ python manage.py runserver